---
title: 'OpenGJK'
date: 2020-5-14T15:14:39+10:00
---

A fast and robust C implementation of the Gilbert-Johnson-Keerthi (GJK) algorithm with interfaces for C#, Go, Matlab, Python and Unity