---
title: 'Examples'
date: 2020-02-11T19:27:37+10:00
weight: 3
---

[The GitHub repository](https://github.com/MattiaMontanari/openGJK) contains may examples on how to invoke OpenGJK from various languages and packages. These are listed and briefly described below.

## C and C++

After cloning the repository, open a terminal window in the parent folder of OpenGJK and type:

``` sh
cmake -E make_directory build
cmake -E chdir build cmake -DCMAKE_BUILD_TYPE=Release ..
cmake --build build
cmake -E chdir build/examples/c/ ./example_lib_opengjk_ce
```

If you get an error, make sure you have CMake and a modern C compiler.

If you want the C++ version of this example, simply compile with a C++ compiler.

## C#

This example is based on Mono and is the starting point for calling [OpenGJK in Unity](https://github.com/MattiaMontanari/urban-couscous). Moreover, it will show how to compiler OpenGJK in single precision.

After cloning the repository, open a terminal window in the parent folder of OpenGJK and type:

``` sh
cmake -E make_directory build
cmake -E chdir build cmake -DCMAKE_BUILD_TYPE=Release -DBUILD_MONO=ON -DSINGLE_PRECISION=ON ..
cmake --build build --target opengjk_ce
cmake -E chdir build make install
cd examples/cs/ && mcs -out:main.exe main.cs && mono main.exe
```

## Go

Golang is a powerful tool and this example was contributed by someone on Github! The current Go version on which this is tested is 1.15.

After cloning the repository, open a terminal window in the parent folder of OpenGJK and type:

``` sh
cd examples/go/openGJK 
go build
go test -v
```

## Matlab and Octave

If you have Matlab installed, in the command line type the following

``` sh
cd exaples/matlab
matlab -nosplash -nodesktop -wait -r "runme"
```

The first run will take a while because Matlab compiles a mex file, but you should then see the logo of OpenGJK popping up in your screen:

{{< figure src="/images/gjkmatlabdemo.png"   width="80%">}}

Alternatively, with Octave run:
``` sh
cd examples/matlab/
octave runme.m
```

In this case there will be no graphical output.

## Python

We explored three different binding solutions between C and Python: PyBind11, Ctypes and Cython.

### PyBind11
The C functions are wrapped using `PyBind11` in an C++ implementation created by Dr Chris Richardson. 
This also uses  `eigen`, making the interface between data type very efficient and easy to understand. 

The implementation has not been merge into the master branch, but can be found [here](https://github.com/MattiaMontanari/openGJK/tree/master/examples/python).

Some of the tests on OpenGJK use this Python interface and rely on `pytest`. Once this is installed, you can run
``` python
python3 -m pytest -s -v test.py
```  
A full example, including compiling script, can be found in this [GitHub CI file](https://github.com/MattiaMontanari/openGJK/blob/master/.github/workflows/github-opengjk-examples.yml).

### Cython

These are currently works in progress and the idea is to provide a clearer overview of what can be achieved with these libraries. As before, examples including compiling scripts, can be found in this [GitHub CI file](https://github.com/MattiaMontanari/openGJK/blob/master/.github/workflows/github-opengjk-examples.yml).
