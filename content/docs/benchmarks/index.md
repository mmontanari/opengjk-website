---
title: 'Benchmarks'
date: 2023-04-11T19:27:37+10:00
weight: 3
---

The OpenGJK library is continuously for accuracy and performance. This page briefly shows the result of the benchmark that test the runtime performance of the library.
This page is updated each time a new version of the library is released. It's a rudimentary dashboard that quickly summarises where recent development has lead to - in terms of speedup.

<!-- I yet have to see modern C++ beating C at runtime. -->

## Latest run

The latest benchmarks ran on a shared virtual machine.

{{< rawhtml >}}
<br>
Latest run on: <p id="date">Loading data ... </p>
Number of v-CPU: <p id="num_cpus">Loading data ... </p>
CPU(s) frequency in MHz: <p id="mhz_per_cpu">Loading data ... </p>

</br>

{{< /rawhtml >}}

### Results

The OpenGJK is called for three simple scenario:

- A pair of points
- A pair of random polytopes with 100 vertices each
- A pair of random polytopes with 1000 vertices each

For each scenario the mean, median, standard deviation and [coefficient of variation](https://en.wikipedia.org/wiki/Coefficient_of_variation) (cv) is reported for the runtime (CPU time). The results are reported below in logarithmic scale.

{{< rawhtml >}}

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

<script src="/js/wc.js"></script>

<div id="chart_mattia"></div>

<!-- <span id='colchart_before' style='width: 450px; height: 250px; display: inline-block'></span> -->
<!-- <span id='colchart_after' style='width: 450px; height: 250px; display: inline-block'></span> -->
<!-- <span id='colchart_diff' style='width: 450px; height: 250px; display: inline-block'></span> -->
<!-- <span id='barchart_diff' style='width: 450px; height: 250px; display: inline-block'></span> -->

{{< /rawhtml >}}

## Performance changelog

### Between v3.0.3 and v4.0.0

The runtime was reduced by 12% by introducing the `restrict` in the pointer declarations of OpenGJK.

{{< chart 111 200 >}}
{
    type: 'bar',
    data: {
        labels: ['v3.0.3', 'v4.0.0'],
        datasets: [{
            data: [3.04, 2.53],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        plugins: {
            legend: {
                display: false,
                }
            },
        maintainAspectRatio: false,
        indexAxis: 'y',
        scales: {
            x: {
            title: {
                display: true,
                text: 'Wall time [us]'
            }},
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
}
{{< /chart >}}

*Test informations: Ran on Intel(R) Core(TM) i9-9980HK CPU @ 2.40GHz (L1 Data 32 KiB, L1 Instruction 32 KiB, L2 Unified 256 KiB (x8), L3 Unified 16384 KiB) and build with gcc 12.2.0 using the CMake Release flags.*