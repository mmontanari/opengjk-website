---
title: 'Showcases'
date: 2019-02-11T19:27:37+10:00
weight: 2
---

This page shows how OpenGJK is used, or called, from within Neper, PyBullet and Unity. Get in touch if you know of other case studies that should complete this list.

## Neper

[Neper](http://www.neper.info/) is a software package for polycrystal generation and meshing. It can deal with 2D and 3D polycrystals with a very large number of grains. This software is used extensively to model morphologies obtained from experimental inspections, including X-Rays.

{{< figure src="/images/morphotarget.png"   width="200px" >}} 

OpenGJK was brought into Neper to speedup the measure the distance between voxels and cells. The same results (minimum value of *f* ) where obtained 25% faster with OpenGJK (light blue) than the original implementation (red).
 
{{< figure src="/images/neper.png" width="50%" >}} 
 
## PyBullet

[PyBullet](https://pybullet.org/wordpress/) is a renown real-time physics engine which simulates collision detection, soft and rigid body dynamics. It is used in video games as well as for visual effects in movies and more recently has been applied for robotics simulations.

In the examples  below hundreds of tiny barrels hit an obstacle and fall down. The video compares to real-time simulations using the original pyBullet (left) and the modified version with openGJK (right).
At around sec 35 the new version completes the simulation, whereas the original version needs few more seconds to complete.
 
{{< youtube id="ULjVh6Sb_4E" >}} 

The integration of openGJK in pyBullet is not something I'm planning to do in the near future. Erwin has been very helpful but there is still few things to do --- reach out if you want to contribute!

## Unity

You can call OpenGJK to compute the minimum distance between to Game Objects in Unity. I created a rudimentary Plugin for Unity and a couple of test scenes to get you started.

The sample scenes that aim to demonstrate how to call OpenGJK in your project. It is quite unlikely that the scripts provided here will fit every single project in need for a GJK implementation, however these scripts are trivial to understand. Use them to learn and rewrite your own to suit your project.

{{< figure src="/images/scene_2.gif" >}}

In the example above the spheres are rigid, but bounce up thanks to a force applied upward. This magnitude of this force is proportional to the distance, computed with openGJK, between the plan and each sphere.
