---
title: 'Overview'
date: 2019-02-11T19:27:37+10:00
weight: 1
---

OpenGJK is free and received contributions from many people. The development started in 2015 at Oxford University, with the goal of bringing GJK into engineering simulations. However all existing versions of GJK, or similar, algorithms did not provide a sufficient level of accuracy and robustness.
An improved method was first published in **ACM Transactions of Graphics** and the same year was one of the three papers referred for **SIGGRAPH 2017**.
The following year the software was release under the LGPL one year later with a publication on **SoftwareX**.

Montanari, M. and Petrinic, N. and Barbieri, E.
[*Improving the GJK Algorithm for Faster and More Reliable Distance Queries Between Convex Objects*](https://doi.org/10.1145/3083724)
(2017). ACM Trans. Graph.

The whole algorithm as been re-designed and re-written later on and this is where the project is today.

## Who is using OpenGJK

Nobody 😄 **but** DeepMind (Google) in MuJoCo, Epic Games in Chaos physics for Unreal and many others have reimplemented their version of OpenGJK.

The most recent and most elegant version I know is from MuJuCo 👏, and it's [available here](https://github.com/google-deepmind/mujoco/blob/476599f8b6175ef5a3040c47ebb3cd99a83c5365/src/engine/engine_collision_gjk.c#L111). In my opinion this is has implementation details that improve speed and, certainly, code readability. It does not seem to be able to support SIMD instructions, but I would not be surprised if they develop that version as closed-source product.

This is how the Signed Volume function looks in their codebase, and it is so nice to see.

![](imgs/2024-09-17-14-52-18.png)

If you have access to the Epic Game Unreal Engine repo, you can see that a much older implementation is available there. That one comes in various flavour - as you would expect from a AAA Game Engine. In their GJK implementation you can see a variant of the Signed Volume method that I presented at SIGGRAPH.

## When should I use openGJK?

The library is designed for accuracy and robustness. It suits scientific computing, robotics and computer graphics applications. As you may know, OpenGJK measures the distance between **any convex polytope**, including:

- line segments
- triangles
- tetrahedrons
- cubes
- could points.

If you need to compute the minimum distance between these objects, than OpenGJK is for you. Should you provide a non-convex object as input to OpenGJK, the computed distance is between the convex hulls of the two bodies.

### Why people reimplement OpenGJK?

Certainly is not fun to rewrite it, but the current license can be a reason for rewriting - and improving - the original Signed Volume method.

## Getting Started

OpenGJK works out of the box on most common platforms and operating systems (Linucs, macOS and Windows). It is well documented, continuously tested and benchmarked.

- Code and a simple *how to compile* guide is [available on GitHub](https://github.com/MattiaMontanari/openGJK).
- Benchmark results are [shown here](../benchmarks/)
- Examples showing how to invoke OpenGJK from C#, Go, Python, Matlab or Unity [are in the examples page](../examples/). There you can also find some of the applications using OpenGJK.
- If you are looking for a API documentation, however trivial it is, [here it is what you are looking for](https://www.mattiamontanari.com/opengjk/docsapi/).

## License

The open-source, also called Community Edition (CE), of OpenGJK is is copyrighted (C) 2017-2020 by Mattia Montanari and distributed under the terms of the [GNU General Public License (GPL) v3](https://www.gnu.org/licenses/gpl-3.0.txt).
You can use OpenGJK free of charge and adhere to the restrictions on its distribution.

If you need a different license, an Enterprise Edition (EE) is available. Please get into directly.