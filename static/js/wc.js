// Load the Visualization API and the piechart package.
google.charts.load("current", { packages: ["corechart"] });

google.setOnLoadCallback(drawChart);
function drawChart() {
  //   Issue data via http
  const xmlhttp = new XMLHttpRequest();

  xmlhttp.onload = function () {
    // Point to data
    const myObj = JSON.parse(this.responseText);

    // Create gchart object
    var data = new google.visualization.DataTable();

    // Declare columns
    data.addColumn("string", "Name");
    // data.addColumn("number", "real_time");
    data.addColumn("number", "cpu_time");
    data.addColumn({ role: "style" });

    // Loop over all rows in benchmarks and create trieds for plotting
    for (var i = 0; i < myObj.benchmarks.length; i++) {
      console.debug(i);
      data.addRow([
        myObj.benchmarks[i].name
          .split("/")[0]
          .concat(" ", myObj.benchmarks[i].name.split("_").pop()),
        // myObj.benchmarks[i].real_time,
        myObj.benchmarks[i].cpu_time,
        "",
      ]);
    }

    var options = {
      height: 600,
      legend: "none",
      bars: "vertical",
      bar: { groupWidth: "75%" },
      title: "CPU time measurements [us]",
      is3D: false,
      hAxis: { logScale: true },
      responsive: true,
      maintainAspectRatio: true,
    };
    var chart = new google.visualization.BarChart(
      document.getElementById("chart_mattia")
    );
    //   document.getElementById("piechart_3d")
    chart.draw(data, options);

    // Extract data from Context
    var context = myObj.context;
    document.getElementById("date").innerHTML = context.date;
    document.getElementById("num_cpus").innerHTML = context.num_cpus;
    document.getElementById("mhz_per_cpu").innerHTML = context.mhz_per_cpu;
  };
  xmlhttp.open(
    "GET",
    "https://mmontanari.gitlab.io/opengjk_3d/benchmark_run_results_ubuntu.json"
  );
  xmlhttp.send();
}
